.. _root:

.. meta::
   :google-site-verification: 1h3YEKIGxsPBNbYQzQZ-DqaHkiMvoBXWSeGW3uWkvbQ

.. image:: rtd_pages/images/zcash-logo-horizontal-fullcolor.png
   :scale: 15
   :align: right

===================
Welcome to Zcash!
===================

This is the home for Zcash documentation for end users and developers. Check out our quickstarts, tutorials, API reference, and code examples.

.. raw:: html

    <div class="button-box-container">
      <div class="button-box bg-orange">
        <a href="https://z.cash/wallets/">
          <div>
            <img src="_static/images/box_wallets_icon.png" />
          </div>
          <div>
            <p>
              <strong>Zcash Wallets </strong>
              <span>A list of wallet apps available for use</span>
            </p>
            <p>https://z.cash/wallets/</p>
          </div>
        </a>
      </div>

     <div class="button-box bg-gray">
       <a href="../latest/rtd_pages/user_guide.html">
         <div>
          <img src="_static/images/box_zcashd_icon.png" />
         </div>
         <div>
           <p>
             <strong>Zcashd Core Platform </strong>
             <span>How to join the Zcash network</span>
           </p>
           <p>Read the Docs</p>
         </div>
       </a>
     </div>

    <div class="button-box bg-lgt-blue">
      <a href="../latest/rtd_pages/zig.html">
        <div>
          <img src="_static/images/box_zig_icon.png" />
        </div>
        <div>
          <p>
            <strong>Integration Guide </strong>
            <span>How to add Zcash to your project</span>
          </p>
          <p>Read the Docs</p>
        </div>
      </a>
    </div>

   <div class="button-box bg-yellow">
     <a href="../latest/rtd_pages/lightclient_support.html">
        <div>
          <img src="_static/images/box_mobile_icon.png" />
        </div>
        <div>
         <p>
           <strong>Mobile Dev Resources </strong>
           <span>Example implementations of SDKs/APIs</span>
         </p>
         <p>Read the Docs</p>
       </div>
     </a>
   </div>

    <div class="button-box bg-yellow">
      <a href="https://zcash-rpc.github.io/">
        <div>
          <img src="_static/images/box_mobile_icon.png" />
        </div>
        <div>
          <p>
            <strong>RPC Documentation </strong>
            <span>Details on zcashd commands</span>
          </p>
          <p>https://zcash-rpc.github.io/</p>
        </div>
      </a>
    </div>

     <div class="button-box bg-lgt-blue">
       <a href="https://discord.gg/PhJY6Pm">
        <div>
          <img src="_static/images/box_community_icon.png" />
        </div>
        <div>
          <p>
           <strong>Community Chat </strong>
           <span>Say hi and see what we're up to</span>
          </p>
          <p>Discord</p>
         </div>
       </a>
     </div>
   </div>

..
  _image:: rtd_pages/images/box_community.png
   :width: 49%
   :target: https://discord.gg/PhJY6Pm

.. toctree::
   :caption: Introduction
   :hidden:

   rtd_pages/basics.md
   rtd_pages/ecosystem.md
   rtd_pages/librustzcash_arch.rst
   rtd_pages/best_practices.md

.. toctree::
   :caption: Interact with Zcash
   :hidden:

   rtd_pages/zcash_wallets.md
   rtd_pages/zig.rst
   rtd_pages/ECC_wallet.md
   rtd_pages/zcashd.md
   rtd_pages/protocol.md

.. toctree::
   :caption: Resources
   :hidden:

   rtd_pages/funding.md
   rtd_pages/learning.md
   rtd_pages/dev_tools.md
